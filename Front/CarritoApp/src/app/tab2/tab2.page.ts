import { Component } from '@angular/core';
import { Cart } from 'src/assets/Models/Cart';
import { Variables } from 'src/assets/variables';

import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  carts: Cart[] = new Array();
  total:number=0;

  constructor(public httpClient: HttpClient) { }

  ionViewWillEnter() {
    this.cargar();
  }

  cargar() {
    this.httpClient.get<Cart[]>(Variables.url + "cart").subscribe(r => {
      this.total=0;
      r.forEach(cart => {
        cart.Producto = Variables.products.filter(m => m.IdProducto == cart.IdProducto)[0];
        this.total+=cart.Precio;
      });
      this.carts = r;
    });
  }

  async eliminar(cart: Cart) {
    this.httpClient.request('delete',Variables.url + "cart",{body:cart}).subscribe(r => {
      this.cargar();
    });
  }
}
