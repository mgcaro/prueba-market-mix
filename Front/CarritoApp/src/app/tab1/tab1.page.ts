import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Variables } from 'src/assets/variables';
import { Product } from 'src/assets/Models/Product';
import { Cart } from 'src/assets/Models/Cart';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  products: Product[] = new Array();


  constructor(public httpClient: HttpClient, public toastController: ToastController) {

  }

  ngOnInit() {
    this.httpClient.get<Product[]>(Variables.url + "product").subscribe(r => {
      this.products = r;
      Variables.products=r;
    });
  }

  async agregar(product: Product) {
    var cart = new Cart();
    cart.Cantidad = 1;
    cart.Precio = product.Precio;
    //cart.Fecha = new Date();
    cart.IdProducto = product.IdProducto;

    this.httpClient.post(Variables.url + "cart", cart).subscribe(r => {
      alert("Producto Agregado");
    });
  }

}
