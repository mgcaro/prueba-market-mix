export class Product {
    IdProducto: number;
    Nombre: string;
    Descripcion: string;
    Precio: number;
    Imagen: string;
}