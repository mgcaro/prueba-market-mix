import { Product } from './Product';

export class Cart {
    IdCarrito: number;
    Cantidad: number;
    Precio: number;
    Fecha: Date;
    IdPersona: number;
    IdProducto: number;
    Producto:Product;
}