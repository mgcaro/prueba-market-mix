const express = require("express");
const bodyParser = require('body-parser');
const app = express();
const mysql = require('mysql');

//Conexión con la base de datos
var connection = mysql.createConnection({
    host: 'zolem.com',
    user: 'marketmix',
    password: 'marketmix.123',
    database: 'Carrito'
});

connection.connect();

//se configura express para que utilice la libreria bodyParser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(function(req, res, next) {
    //Permitir desde cualquier origen el request
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*, *, *, *");
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    next();
});

//Se crea la variable cart para parsear el json que viene en el request.
let cart = {
    IdCarrito: 0,
    Cantidad: 0,
    Precio: 0,
    Fecha: '',
    IdPersona: 0,
    IdProducto: 0,
};

let respuesta = {
    valor: ''
}

app.route("/cart")
    //Se muestran todos los elementos de la tabla Carrito.
    .get(function(req, res) {
        connection.query("SELECT * FROM Carrito", function(err, rows, fields) {
            res.send(rows);
        });
    })
    //Se inserta en la tabla Carrito un nuevo elemento.
    .post(function(req, res) {
        console.log(req.body)
        cart = req.body
        connection.query("insert into Carrito (Cantidad,IdProducto,Precio) VALUES(" + cart.Cantidad + "," + cart.IdProducto + "," + cart.Precio + ") ", function(err, rows, fields) {
            respuesta = {
                valor: "Agregado"
            }

            res.send(respuesta);
        });
    })
    //Se elimina de la tabla Carrito un elemento existente.
    .delete(function(req, res) {
        cart = req.body
        connection.query("delete from Carrito where IdCarrito=" + cart.IdCarrito, function(err, rows, fields) {
            respuesta = {
                valor: "Eliminado"
            }

            res.send(respuesta);
        });
    })

app.route("/product")
    //Se muestran todos lo elementos de la tabla Producto.
    .get(function(req, res) {
        connection.query("SELECT * FROM Producto", function(err, rows, fields) {
            res.send(rows);
        });
    })


//Puerto por el cual se inicializa el servidor.
app.listen(3000, () => {
    console.log("El servidor está inicializado en el puerto 3000");
});